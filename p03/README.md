# Práctica 03
## Reportes, colas y pilas
El objetivo es:
* Tener una forma de generar un reporte de los errores que cometemos
 al momento de programar.
* Seguir buenas prácticas de programación siguiendo este reporte.
* Conocer la practicidad de usar una cola y una pila en la resolución
 de problemas.

## Problemática
### Reportes (Primera parte)
Actualmente no se cuenta con un mecanismo automático que nos permita 
identificar errores y malas prácticas de programación.

Como una herramienta de apoyo para agilizar las revisiones y ayuda 24/7
para los alumnos, se agregará al proyecto actual y futuros la 
herramienta que trae por omisión Maven para la generación de reportes 
relacionados al [estilo de código](https://checkstyle.org/).

Existen [limitaciones de esta herramienta](https://checkstyle.org/writingchecks.html#Limitations),
pero en la mayoría de los casos nos guía a seguir buenas prácticas 
de programación.

Para generar el reporte basta con moverse al directorio donde se 
localiza el `pom.xml` y ejecutar el comando:
```bash
mvn checkstyle:checkstyle
```
Esto generará el archivo `target/site/checkstyle.html` mismo que
contendrá el reporte de los errores encontrados.

![picture](imgs/html-checkstyle.png)

Para acceder desde el navegador puedes escribir en la barra de direcciones:
```text
file:///home/tu-usuario/ruta-de-prácticas/practicas-edatos-20202/p03/target/site/checkstyle.html
```

Existe otra forma de acceder a este reporte con mejor detalle, generando
el sitio de nuestro proyecto.

Para esto es necesario el plugin correspondiente en el *pom* del proyecto:

1. En la propiedad `<reporting>` agregar el plugin **maven-checkstyle-plugin**,
el cual se especifica aquí:
  ```xml
  <plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-checkstyle-plugin</artifactId>
   <version>3.1.1</version>
   <reportSets>
     <reportSet>
       <reports>
         <report>checkstyle</report>
       </reports>
     </reportSet>
   </reportSets>
  <plugin>
  ```

2. Generar el sitio del proyecto:
  ```bash
  mvn clean site
  ```

3. Acceder por el navegador a:
  ```text
  file:///home/tu-usuario/ruta-de-prácticas/practicas-edatos-20202/p03/target/site/summary.html
  ```

  3.a. Aparecerá la siguiente ventana:
  
  ![picture](imgs/site-summary.png)

  3.b. Después, ir a la barra de navegación izquierda y dar click en **Project Reports**:
  
  ![picture](imgs/checkstyle-report.png)
  
  En esta página puedes visualizar el reporte generado por el checkstyle.



### Torres de Hanoi (Segunda parte)

Un Gamer ansioso por poner a prueba su destreza para mover piezas
siguiendo un conjunto de reglas, les pide que desarrollen un juego
basado en las Torres de Hanoi.

Este juego consiste de tres torres y un número finito de discos que
tienen un peso asociado de *1* a *n*. El juego inicia en la torre
**origen** con todos los discos apilados de **menor a mayor**. <u>El
objetivo</u> es pasar todos los discos a la torre **destino** utilizando la
torre **auxiliar**; la única regla para mover los discos, es que ningún
disco *"mayor"* sea apilado sobre un disco *"menor"*.

Al jugador se le pregunta el número de discos con los que desea jugar, él
puede escoger desde *1* hasta *n*. Es necesario que cuando el jugador
quiera realizar un movimiento inválido el programa le informe las reglas
que debe seguir y además asegurar que el movimiento no se realice.

Como esta es una implementación que utilizará el Equipo Nacional de Torres de
Hanoi (ENTOHA) para entrenar y participar en el Abierto Mundial de Torres
de Hanoi este juego, al finalizar, deberá entregar en informe de
movimientos realizados por el jugador con el fin de que el entrenador
del equipo pueda analizar la información.

Este juego puede ser desarollador para *Consola* o hacer uso de una
*UI*.


## Puntos a evaluar
Adicional a los puntos mencionados en las **Especificaciones 
generales de entrega de prácticas v.0.2**, se tomarán en cuenta:

**Para la parte uno**:
* Historial de commits siguiendo buenas prácticas.
* Resolución completa de los errores marcados por el checkstyle.

**Para la parte dos**:

* Sin errores en el checkstyle.
* Las razones en el diseño de la solución del problema a resolver:
  * Estructuras de datos.
  * Clases para leer y procesar la información del usuario (jugador).
  * Clases de soporte para la aplicación.

* La correcta ejecución del programa para el *happy path*.
